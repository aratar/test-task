Mějme 2 pole objektů:
Příchozí a odchozí platby stažené z bank na konci měsíce, načtené z formátu .csv
Příchozí a odchozí platby stahované z bank průběžně a ukládané do databáze aplikace

    id - identifikátor (vzájemně rozdílné napříč systémy)
    date - datum platby
    varSym - jednoznačný identifikátor osoby (Rodné číslo)
    amount - částka (Kč)

    // Příchozí a odchozí platby stažené z bank na konci měsíce, načtené z formátu .csv
    $csvPayments = [
       (object)['id' => 1, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => 1000],
       (object)['id' => 2, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => 1500],
       (object)['id' => 8, 'date' => '2021-11-02', 'varSym' => '3333334444', 'amount' => -8000],
       (object)['id' => 27, 'date' => '2021-11-01', 'varSym' => '5555556666', 'amount' => 1500],
       (object)['id' => 64, 'date' => '2021-11-02', 'varSym' => '1111112222', 'amount' => -1600],
       (object)['id' => 65, 'date' => '2021-11-03', 'varSym' => '1111112222', 'amount' => 500],
        (object)['id' => 92, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => -1600],
       (object)['id' => 155, 'date' => '2021-11-01', 'varSym' => '9999990000', 'amount' => 9999],
    ];

    // Příchozí a odchozí platby stahované z bank průběžně a ukládané do databáze aplikace
    $dbPayments = [
        (object)['id' => 13, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => -1600],
        (object)['id' => 24, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => 1000],
        (object)['id' => 25, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => 1500],
        (object)['id' => 33, 'date' => '2021-11-02', 'varSym' => '1111112222', 'amount' => -1600],
        (object)['id' => 91, 'date' => '2021-11-03', 'varSym' => '1111112222', 'amount' => 500],
        (object)['id' => 127, 'date' => '2021-11-02', 'varSym' => '3333334444', 'amount' => -8000],
        (object)['id' => 202, 'date' => '2021-11-01', 'varSym' => '5555556666', 'amount' => 1500],
        (object)['id' => 208, 'date' => '2021-11-01', 'varSym' => '8888880000', 'amount' => 8888],
    ];

Napárujte platby na sebe. Zjistěte jaké platby v jakých systémech přebývají / scházejí a ty zobrazte.
