<?php
/**
 * Napárujte na sebe. Zjistěte jaké platby v jakých systémech přebývají / scházejí a ty zobrazte.
 * 
 * id - identifikátor (vzájemně rozdílné napříč systémy)
 * date - datum platby
 * varSym - jednoznačný identifikátor osoby (Rodné číslo)
 * amount - částka (Kč) 
 */


// Příchozí a odchozí platby stažené z bank na konci měsíce, načtené z formátu .csv
$csvPayments = [
    (object)['id' => 1, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => 1000],
    (object)['id' => 2, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => 1500],
    (object)['id' => 8, 'date' => '2021-11-02', 'varSym' => '3333334444', 'amount' => -8000],
    (object)['id' => 27, 'date' => '2021-11-01', 'varSym' => '5555556666', 'amount' => 1500],
    (object)['id' => 64, 'date' => '2021-11-02', 'varSym' => '1111112222', 'amount' => -1600],
    (object)['id' => 65, 'date' => '2021-11-03', 'varSym' => '1111112222', 'amount' => 500],
    (object)['id' => 92, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => -1600],
    (object)['id' => 155, 'date' => '2021-11-01', 'varSym' => '9999990000', 'amount' => 9999],
];

// Příchozí a odchozí platby stahované z bank průběžně a ukládané do databáze aplikace
$dbPayments = [
    (object)['id' => 13, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => -1600],
    (object)['id' => 24, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => 1000],
    (object)['id' => 25, 'date' => '2021-11-01', 'varSym' => '1111112222', 'amount' => 1500],
    (object)['id' => 33, 'date' => '2021-11-02', 'varSym' => '1111112222', 'amount' => -1600],
    (object)['id' => 91, 'date' => '2021-11-03', 'varSym' => '1111112222', 'amount' => 500],
    (object)['id' => 127, 'date' => '2021-11-02', 'varSym' => '3333334444', 'amount' => -8000],
    (object)['id' => 202, 'date' => '2021-11-01', 'varSym' => '5555556666', 'amount' => 1500],
    (object)['id' => 208, 'date' => '2021-11-01', 'varSym' => '8888880000', 'amount' => 8888],
];




// Řešení
// PHP 7.4.0 example -  http://sandbox.onlinephpfunctions.com/code/e07477dc2581b832fa949c50a76e6d5684598a90

$paymentDiffer = new PaymentDiffer($dbPayments, $csvPayments, ['date', 'varSym', 'amount']);
echo "Přebývající:";
print_r($paymentDiffer->getAbove());
echo "Scházející:";
print_r($paymentDiffer->getUnder());

// Nejsem si jistý, že date+varSym+amount unikátní kombinace,
// proto jsem dodal collisionResolve funkci

class PaymentDiffer
{
    /**
     * The array to compare from
     * @var object[]
     */
    private array $compareFrom;

    /**
     * Array to compare against
     * @var object[]
     */
    private array $compareAgainst;

    /**
     * @param object[] $compareFrom
     * @param object[] $compareAgainst
     * @param string[] $fields
     */
    public function __construct(array $compareFrom, array $compareAgainst, array $fields)
    {
        $this->compareFrom = $this->prepareHashedObjects($compareFrom, $fields);
        $this->compareAgainst = $this->prepareHashedObjects($compareAgainst, $fields);
    }

    /**
     * @return object[]
     */
    public function getAbove(): array
    {
        return $this->diff($this->compareFrom, $this->compareAgainst);
    }

    /**
     * @return object[]
     */
    public function getUnder(): array
    {
        return $this->diff($this->compareAgainst, $this->compareFrom);
    }

    /**
     * @param object $compareFromItem
     * @param string[] $fields
     * @return string
     */
    private function hash(object $compareFromItem, array $fields): string
    {
        $hashSource = [];

        foreach ($fields as $field) {
            $hashSource[] = $compareFromItem->$field;
        }

        return md5(implode(':', $hashSource));
    }

    /**
     * @param object[] $objects
     * @param string[] $fields
     * @return array
     */
    private function prepareHashedObjects(array $objects, array $fields): array
    {
        $hashed = [];

        foreach ($objects as $object) {
            $hashIndex = $this->collisionResolve($hashed, $this->hash($object, $fields));
            $hashed[$hashIndex] = $object;
        }

        return $hashed;
    }

    /**
     * @param object[] $hashed
     * @param string $hash
     * @return string
     */
    private function collisionResolve(array $hashed, string $hash): string
    {
        if (!isset($hashed[$hash])) {
            return $hash;
        }

        return $this->collisionResolve($hashed, md5($hash));
    }

    /**
     * @param object[] $compareFrom
     * @param object[] $compareAgainst
     * @return object[]
     */
    private function diff(array $compareFrom, array $compareAgainst): array
    {
        return array_values(array_diff_key($compareFrom, $compareAgainst));
    }
}
